#ifndef MOVIESDETECTOR_H
#define MOVIESDETECTOR_H

#include "movieobj.h"
#include <list>
#include <vector>
#include <filesystem>

class MoviesDetector
{
public:
    MoviesDetector();
    std::vector<MovieObj> start(std::string root_path);
    std::string root_path;
    std::vector<std::string>  all_directories;
     std::vector<MovieObj> MovieList;

    void extract_movies(std::string path);
    bool is_bluray_or_dvd(std::string path);
    bool in_list(std::string text, std::list<std::string> & list);
    
private:
    
    
   



};
#endif // MOVIESDETECTOR_H