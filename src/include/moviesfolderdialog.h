#ifndef MOVIESFOLDERDIALOG_H
#define MOVIESFOLDERDIALOG_H
#include <iostream>
#include <gtkmm/dialog.h>
#include <gtkmm/builder.h>
#include <glibmm/refptr.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/button.h>
#include <giomm/settings.h>
class MoviesFolderDialog : public Gtk::Dialog
{
public:
    MoviesFolderDialog(BaseObjectType* cobject ,Glib::RefPtr<Gtk::Builder>& refGlade);
    virtual ~MoviesFolderDialog();
     
protected:
    // gtk builder
    Glib::RefPtr<Gtk::Builder> builder;

    // gtk treeview
    Gtk::TreeView *treeview;

    //list store model
    Glib::RefPtr<Gtk::ListStore> liststore;
    Gtk::TreeModel::Row row;

    // Application Setting
    Glib::RefPtr<Gio::Settings> settings;
    std::vector<Glib::ustring> folder_list;
    
    // My Buttons
    Gtk::Button *btn_add;
    Gtk::Button *btn_remove;
    Gtk::Button *btn_close;
    Gtk::Button *btn_save;
   
    // add row to the tree
    void addrow(std::string path);


    // signals
    // butons signals
    void on_btn_add_clicked();
    void on_btn_remove_clicked();
    void on_btn_close_clicked();
    void on_btn_save_clicked();
    // window signals
    void on_window_showed();

    // Tree Model columns
    class ModelsColumns : public Gtk::TreeModel::ColumnRecord
    {
    public:
        ModelsColumns() { add(mc_path); }
        Gtk::TreeModelColumn<std::string> mc_path;

    };

    ModelsColumns m_columns;
};

#endif // MOVIESFOLDERDIALOG_H