#ifndef MOVIESOBJ_H
#define MOVIESOBJ_H

#include <string>

class MovieObj
{
public:
    MovieObj(std::string name, std::string path, bool isF) { m_name=name, m_path=path, isFolderMove=isF;}


    std::string m_name;
    std::string m_path;

    bool isFolderMove;
};

#endif // MOVIESOBJ_H
