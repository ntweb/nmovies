#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>
#include <glibmm/refptr.h>
#include <gtkmm/application.h>
#include <gtkmm/button.h>
#include <gtkmm/window.h>
#include <gtkmm/builder.h>
#include <gtkmm/label.h>
#include <gtkmm/treeview.h>
#include "moviesdetector.h"
#include "moviesfolderdialog.h"
#include "movieobj.h"
class MainWindow : public Gtk::Window
{
public:
    MainWindow(BaseObjectType* cobject ,Glib::RefPtr<Gtk::Builder>& refGlade);
    virtual ~MainWindow();  
private:
    MoviesDetector md;
    std::vector<MovieObj> MoviesList;

protected:
    // gtk builder
    Glib::RefPtr<Gtk::Builder> builder;

    // member widget
    Gtk::Button* btn_moviesfolders;
    Gtk::Button* btn_detector;
    Gtk::Button* btn_play;
    Gtk::Label* lbl_statusbar;
    Gtk::TreeView *treeview;

    
    // signal handler
    void on_btn_moviesfolders_clicked();
    void on_btn_detector_clicked();

    // windows
    MoviesFolderDialog *mfd;

    // Application Setting
    Glib::RefPtr<Gio::Settings> settings;
    std::vector<Glib::ustring> folder_list;


    //list store model
    Glib::RefPtr<Gtk::ListStore> liststore;
    Gtk::TreeModel::Row row;

    // My Members
    std::vector<MovieObj> movies_list;

    // my functions
    void update_treeview();








    // Tree Model columns
    class ModelsColumns : public Gtk::TreeModel::ColumnRecord
    {
    public:
        ModelsColumns() { add(mc_name); add(mc_path); }
         Gtk::TreeModelColumn<std::string> mc_name;
        Gtk::TreeModelColumn<std::string> mc_path;

    };

    ModelsColumns m_columns;

    
};
#endif // MAINWINDOW_H