#include "mainwindow.h"
#include "moviesfolderdialog.h"
#include <gtkmm/messagedialog.h>
MainWindow::MainWindow(BaseObjectType* cobject, Glib::RefPtr<Gtk::Builder>& refGlade):
 Gtk::Window(cobject), builder(refGlade), mfd(nullptr)
{

    // get widget from ui
    builder->get_widget("btn_moviesfolders",btn_moviesfolders);
    builder->get_widget("btn_detector",btn_detector);
    builder->get_widget("btn_play",btn_play);
    builder->get_widget("lbl_statusbar",lbl_statusbar);
    refGlade->get_widget("treevie_main_window",treeview);
    // handle signal
    btn_moviesfolders->signal_clicked().connect(sigc::mem_fun(*this,&MainWindow::on_btn_moviesfolders_clicked));
    btn_detector->signal_clicked().connect(sigc::mem_fun(*this,&MainWindow::on_btn_detector_clicked));
    
    // resize the window
    resize(600,400);
    
    //application setting
    settings = Gio::Settings::create("info.ntweb.nmovies");



    // configure ListView and link it with liststore
    liststore = Gtk::ListStore::create(m_columns);
    treeview->set_model(liststore);
    treeview->append_column("Movie name", m_columns.mc_name);
    treeview->append_column("Movie Path", m_columns.mc_path);
}

MainWindow::~MainWindow()
{
    delete mfd;
}

void MainWindow::on_btn_moviesfolders_clicked()
{
    
    builder->get_widget_derived("movies_folders_dialog", mfd);
    
    mfd->set_transient_for(*this);
    mfd->run();
    mfd->hide();
    //delete mwd;
}
void MainWindow::on_btn_detector_clicked()
{
    
    // Clear all rows from Moedl and load the saved one from GSettings
   folder_list.clear();
   folder_list = settings->get_string_array("folder-list");
   if (folder_list.size() == 0) {
       Gtk::MessageDialog msg(*this, "The are no movies folder!");
       msg.set_secondary_text("Click on Movies folders button to add new movies folder");
       msg.run();
   }
   else
   {
       lbl_statusbar->set_text("Reading Movies Folder ... please wait");
       
        for (auto & folder : folder_list)
        {
            MoviesDetector m;
            std::cout << "Start analysing folder: " << folder << std::endl;
            movies_list = m.start(folder);
            //std::cout << "there are " << sample.size() << "Movies" << std::endl;
        }
        lbl_statusbar->set_text("Done .. found " + std::to_string(movies_list.size()) + " Movies" );
        update_treeview();
   }
   
   
}


void MainWindow::update_treeview()
{
    liststore->clear();
    for (auto &movie : movies_list)
    {
       
        row = *(liststore->append());
        row[m_columns.mc_path] = movie.m_path;
        row[m_columns.mc_name] = movie.m_name;
    }
}