#include "moviesfolderdialog.h"
#include <gtkmm/filechooserdialog.h>

MoviesFolderDialog::MoviesFolderDialog(BaseObjectType* cobject, Glib::RefPtr<Gtk::Builder>& refGlade):
Gtk::Dialog(cobject), builder(refGlade), treeview(nullptr)
{
    //Get the Glade-instantiated TreeView
    refGlade->get_widget("treeview_movies_folders_dialog",treeview);

    // configure ListView and link it with liststore
    liststore = Gtk::ListStore::create(m_columns);
    treeview->set_model(liststore);
    treeview->append_column("Movies Path", m_columns.mc_path);
   
    // get refrence to buttons
    refGlade->get_widget("btn_add",btn_add);
    refGlade->get_widget("btn_remove",btn_remove);
    refGlade->get_widget("btn_close",btn_close);
    refGlade->get_widget("btn_save",btn_save);

    // handle signal
    btn_add->signal_clicked().connect(sigc::mem_fun(*this,&MoviesFolderDialog::on_btn_add_clicked));
    btn_remove->signal_clicked().connect(sigc::mem_fun(*this,&MoviesFolderDialog::on_btn_remove_clicked));
    btn_close->signal_clicked().connect(sigc::mem_fun(*this,&MoviesFolderDialog::on_btn_close_clicked));
    btn_save->signal_clicked().connect(sigc::mem_fun(*this,&MoviesFolderDialog::on_btn_save_clicked));
    signal_show().connect(sigc::mem_fun(*this, &MoviesFolderDialog::on_window_showed));

    //application setting
    settings = Gio::Settings::create("info.ntweb.nmovies");
   
   
    

}

MoviesFolderDialog::~MoviesFolderDialog()
{
    
}
void MoviesFolderDialog::on_btn_add_clicked()
{
    Gtk::FileChooserDialog dialog("Please choose a folder", Gtk::FILE_CHOOSER_ACTION_SELECT_FOLDER);
    dialog.set_transient_for(*this);
    //Add response buttons the the dialog:
    dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    dialog.add_button("Select", Gtk::RESPONSE_OK);
    int result = dialog.run();
    if (result == Gtk::RESPONSE_OK)
        addrow(dialog.get_filename());
    
}
void MoviesFolderDialog::on_btn_save_clicked()
{
    // looping throw listtore and store the value into folder_list
    folder_list.clear();
    auto children = liststore->children();
    for (auto iter = children.begin(); iter != children.end(); ++iter )
    {
        auto row = *iter;
        folder_list.push_back(row.get_value(m_columns.mc_path));
    }

    settings->set_string_array("folder-list", folder_list);
    hide();
    
}
void MoviesFolderDialog::on_btn_close_clicked()
{
    hide();
}
void MoviesFolderDialog::on_btn_remove_clicked()
{
    auto selection = treeview->get_selection();

    if (selection)
    {
        auto iter = selection->get_selected();
        if (iter)
        {
            liststore->erase(iter);
        }
    }
}
void MoviesFolderDialog::addrow(std::string path)
{
    row = *(liststore->append());
    row[m_columns.mc_path] = path;
    // std::cout << path;
}

void MoviesFolderDialog::on_window_showed()
{
   // Clear all rows from Moedl and load the saved one from GSettings
   folder_list.clear();
   liststore->clear();
   folder_list = settings->get_string_array("folder-list");
   for (auto & folder : folder_list)
   {
        row = *(liststore->append());
        row[m_columns.mc_path] = folder;
   }
}