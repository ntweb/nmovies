#include <gtkmm/application.h>
#include <gtkmm/builder.h>
#include <glibmm/markup.h>
#include <glibmm/fileutils.h>
#include <iostream>
#include <filesystem>

#include "mainwindow.h"
#include "moviesdetector.h"


int main(int argc, char *argv[])
{
    auto app = Gtk::Application::create(argc, argv, "info.ntweb.nmovies");
    auto refBuilder =Gtk::Builder::create_from_resource("/info/ntweb/nmovies/nmovies.glade");



    // call the main window
    MainWindow* mainWindow = nullptr;
    refBuilder->get_widget_derived("main_window", mainWindow);
    
    app->run(*mainWindow);
    delete mainWindow;

    return 0;
}
