#include "moviesdetector.h"
#include <iostream>
#include <algorithm>

using namespace std;
MoviesDetector::MoviesDetector()  
{
    all_directories.clear();
}


std::vector<MovieObj> MoviesDetector::start(std::string r_path) 
{
    root_path = r_path;
    extract_movies(root_path);
    while (all_directories.size() > 0)
    {
        int length  = all_directories.size();
        for (int i=length-1;  i >= 0; i--)
        {
            extract_movies(all_directories.at(i));
            all_directories.erase(all_directories.begin()+i);
        }
    }
    
    return MovieList;

}


void MoviesDetector::extract_movies(string path)
{
    filesystem::path dir{path};
    list<string> extention = {".mkv",".mp4",".avi"};
    list<string> exlude_folder = {"sample","extras","featurettes","bonus","deleted"};
    list<string> exlude_movie = {"RARBG.com.mp4","sample","directory","etrg.mp4"};
    // read all files and dirs inside given path to analyse
    if (filesystem::exists(dir))
    {
        for (const auto& file : filesystem::directory_iterator(dir))
        {            
            string file_path = file.path().string();
            if (file_path.at(0) == '.')
                continue;
            if (filesystem::is_regular_file(file))
            {
                if ((in_list(file_path.substr(file_path.size() - 4),extention)) && (!in_list(file_path, exlude_movie)) && (!in_list(file_path, exlude_folder)) )
                {
                    MovieObj m (file_path, file_path, true);
                    MovieList.push_back(m);
                }
            }
            else
            {
                if (!in_list(file_path,exlude_folder))
                {
                    if (is_bluray_or_dvd(file_path))
                    {
                        MovieObj m(file_path, file_path, false);
                        MovieList.push_back(m);
                    }
                    else 
                    {
                        all_directories.push_back(file_path);
                    }
                }
            } 
            
        }

    } // endif exeist

}

bool MoviesDetector::is_bluray_or_dvd(string path)
{
    filesystem::path p{path};
    list<string> bluray_dvd = {"bdmv","video_ts"};
    for (const auto& file : filesystem::directory_iterator(p))
    {
        if (in_list(file.path().string(), bluray_dvd))
        {
            return true;
        }
    }
    return false;
}



bool MoviesDetector::in_list(string text, list<string> &list)
{
    //cout << "inlist(" << text << ")" <<endl;
    for (auto item : list)
    {
        // convert item to lower case!
        std::transform(text.begin(), text.end(), text.begin(), ::tolower); 

        if (text.find(item) !=  string::npos) 
            return true;
    }
    return false;
}